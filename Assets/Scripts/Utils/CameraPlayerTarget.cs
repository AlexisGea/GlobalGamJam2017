﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraPlayerTarget : MonoBehaviour {

    public GameObject player;
    public bool is2DMode = true;
    public float lerpFactor = 0.4f;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (is2DMode)
        {
            Vector3 playerPostion = player.transform.position;
            playerPostion.z = transform.position.z;
            Vector3 newPostion = Vector3.Lerp(transform.position, playerPostion, lerpFactor);
            transform.position = newPostion;

        }
        else
        {
            Vector3 playerPostion = player.transform.position;          
            Vector3 newPostion = Vector3.Lerp(transform.position, playerPostion, lerpFactor);
            transform.position = newPostion;
        }
	 
	}
}
