﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class LogoStartGameScript : MonoBehaviour {

    public float waitingForStart=5;
    public float waitingTimeForLogoView = 5;
    public float waitingTimeToDisplay = 2;
    public float waitingTimeToHidder = 2;


    public Image logo;


    private enum States
    {
        START,
        DISPLAY,
        VIEW_LOGO_WAIT,
        HIDDEN,
        HIDDEN_LOGO_WAIT,
        END
    }

    private States state = States.START;

    private bool isWaiting = false;

	// Use this for initialization
	void Start () {

        switch (state)
        {
            case States.START:
                if (!isWaiting)
                    StartCoroutine(WaitForStart());
                else
                {
                    state = States.DISPLAY;
                    isWaiting = false;
                }
                    
                break;
            case States.DISPLAY:
                Color color = logo.color;
                if (ChangeAlphaProgressif(false))
                {
                    state = States.VIEW_LOGO_WAIT;
                }
                                break;
            case States.VIEW_LOGO_WAIT:
                break;
            case States.HIDDEN:
                if (ChangeAlphaProgressif(true))
                {
                    state = States.HIDDEN_LOGO_WAIT;
                }
                break;
            case States.HIDDEN_LOGO_WAIT:
                break;
            case States.END:
                break;
        }
		
	}
	
    public bool ChangeAlphaProgressif(bool hidden)
    {
        Color color = logo.color;
        bool result = false;
        if (hidden)
        {
            if (color.a <= 1)
            {
                color.a = 0;

                result = true;
            }
            else
            {
                color.a -= (255 / waitingTimeToDisplay) * Time.deltaTime;
            }
            logo.color = color;
        }
        else
        {
            if (color.a >= 253)
            {
                color.a = 255;

                result = true;
            }
            else
            {
                color.a += (255 / waitingTimeToDisplay) * Time.deltaTime;
            }
            logo.color = color;
        }
        return result;
    }


	// Update is called once per frame
	void Update () {
		
	}
    
    public IEnumerator WaitForStart()
    {
        yield return new WaitForSeconds(waitingForStart);
        isWaiting = true;
    }
}
